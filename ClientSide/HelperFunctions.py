import paramiko

def XML_FILE_LOCATION():
    return "./bin/JobData.xml"

def STRING_SERVER_DOWNLOAD_OVERRIDE():
    return 

def SERVER_FILES_PATH():
    return "/home/ubuntu/Web_Scraper/ServerSide"

def OpenServerConnection():
    key = paramiko.RSAKey.from_private_key_file("/home/vmdev/Documents/vmpairkey.pem")
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(hostname="13.59.14.189", username="ubuntu", pkey=key)
    return client.open_sftp()

def CloseServerConnection(client):
    client.close()