import tkinter as tk
import tkinter.ttk as ttk 

def PopoutMsg(msg, boxType, posX, posY, width, height):
    popup = tk.Tk()
    g = str(width) + "x" + str(height) + "+" + str(posX) + "+" + str(posY)
    popup.geometry(g)
    popup.wm_title("")

    if (boxType == "label"):
        label = ttk.Label(popup, text=msg)
        label.pack(side="top", fill="x", pady=10)
    elif (boxType == "textbox"):
        textbox = tk.Text(popup)
        textbox.insert("1.0", msg)
        textbox.pack(side="top", fill="x", pady=10)

    B1 = ttk.Button(popup, text="Close", command = popup.destroy)
    B1.pack()
    popup.mainloop()