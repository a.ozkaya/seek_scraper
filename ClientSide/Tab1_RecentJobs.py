import tkinter as tk
import tkinter.ttk as ttk
from CreateTextTable import CreateTextTable
from HelperFunctions import XML_FILE_LOCATION
from PopOut import PopoutMsg
import pyperclip, webbrowser, os
import xml.etree.ElementTree as ET

class Tab_RecentJobs:
    def __init__(self, window, tab, downloadOverride = False):
        self.__window = window
        self.__CreateObjects(tab) 
        self.__UpdateTree() 
        self.__counter = 0
        self.__actions = []
        
#NORMAL FUNCTIONS

    def __UpdateTree(self):
        self.__treeRows = []
        if (os.path.exists(XML_FILE_LOCATION())):
            tree = ET.parse(XML_FILE_LOCATION())
            jobsroot = tree.getroot()
            self.__counter = 0
            for elem in jobsroot:
                if (elem[0].text == "False"):
                    self.__treeRows.append([self.__counter ,elem.attrib['id'], elem[0].text, elem[1].text, elem[2].text, elem[3].text, elem[4].text, elem[5].text,elem[6].text, elem[7].text, elem[8].text, elem[9].text, elem[10].text, elem[11].text])
                    self.__tree.insert('', 'end', str(self.__counter), values=(elem[1].text, elem[3].text, elem[9].text))
                    self.__counter += 1
        else:
            print("XML IMPORT ERROR: " + XML_FILE_LOCATION() + " doesn't exist")

    def __CreateObjects(self, tab):
        self.__tree=ttk.Treeview(tab, height= self.__window.winfo_reqheight() - 155)
        self.__tree['show'] = 'headings'
        self.__tree.bind("<Double-1>", self.__OnDoubleClick)
        

        self.__tree["columns"] = ("1", "2", "3")

        self.__tree.column("1", width=366, stretch=tk.NO)
        self.__tree.column("2", width=40, stretch=tk.NO)
        self.__tree.column("3", width=90, stretch=tk.NO)

        self.__tree.heading("1", text="JobTitle")
        self.__tree.heading("2", text="Loc")
        self.__tree.heading("3", text="Posted Time")
        
        self.buttonSave = ttk.Button(tab, text="Apply", command=self.__ApplyToJobCallBack)
        self.buttonOpenLink = ttk.Button(tab, text="Open URL", command=self.__OpenURLCallBack)
        self.buttonRemove = ttk.Button(tab, text="Remove", command=self.__RemoveCallBack)
        self.undoRemove = ttk.Button(tab, text="Undo", command=self.__UndoCallBack)
        
        self.__tree.grid(row = 1, column = 0, columnspan = 6, sticky = tk.N+tk.W, padx = 10, pady = 2)
        self.buttonSave.grid(row = 0, column = 0, sticky = tk.N+tk.W, padx = (10, 0), pady = 10)
        self.buttonRemove.grid(row = 0, column = 0, sticky = tk.N+tk.W, padx = (100, 0), pady = 10)
        self.buttonOpenLink.grid(row = 0, column = 0, sticky = tk.N+tk.W, padx = (190, 0), pady = 10)
        self.undoRemove.grid(row = 0, column = 0, sticky = tk.N+tk.W, padx = (280, 0), pady = 10)

    def __AddToActions(self, data = None):
        self.__actions.append(data)
        return

    def __RemoveItem(self):
        package = []
        for s in self.__tree.selection():
            _id = None
            #Delete from tree row list
            for tr in self.__treeRows:
                if (tr[0] == int(s)):
                    _id = tr[1]
                    package.append(tr)
                    tr = None
                    break
            #Delete from XML
            tree = ET.parse(XML_FILE_LOCATION())
            jobsroot = tree.getroot()
            jobs = jobsroot.findall("job")
            for j in jobs:
                if (j.attrib["id"] == _id):
                    jobsroot.remove(j)
                    tree.write(XML_FILE_LOCATION())
                    break
            #Remove from Treeview
            self.__tree.delete(s)
            self.__AddToActions(package)
        
# EVENT FUNCTIONS 
        
    def __OnDoubleClick(self, event):
        headers = ["Tree ID", "XML ID", "Rejected", "Job Title", "Category", "Location", "Company", "Contract Type" ,"Pay", "Description 1", "Description 2", "Date", "URL", "Reject Reason"]
        PopoutMsg(CreateTextTable(headers, self.__treeRows[int(self.__tree.focus())], 27), "textbox" ,10 , 10, 490, 520)
    
# BUTTON CALL BACK FUNCTIONS        
        
    def __UndoCallBack(self):
        mainTemp = self.__actions.pop()
        for temp in mainTemp:
            if (temp[2] == "False"):
                #Turn into XML
                tree = ET.parse(XML_FILE_LOCATION())
                jobsroot = tree.getroot()
                job = ET.SubElement(jobsroot, "job")
                rejected = ET.SubElement(job, "rejected")
                jobtitle = ET.SubElement(job, "jobtitle")
                category = ET.SubElement(job, "category")
                location = ET.SubElement(job, "location")
                company = ET.SubElement(job, "company")
                contractType = ET.SubElement(job, "contractType")
                pay = ET.SubElement(job, "pay")
                description1 = ET.SubElement(job, "description1")
                description2 = ET.SubElement(job, "description2")
                postedDate = ET.SubElement(job, "postedDate")
                url = ET.SubElement(job, "url")
                rejectReason = ET.SubElement(job, "rejectReason")
                job.set('id', temp[1])
                rejected.text = temp[2]
                jobtitle.text = temp[3]
                category.text = temp[4]
                location.text = temp[5]
                company.text = temp[6]
                contractType.text = temp[7]
                pay.text = temp[8]
                description1.text = temp[9]
                description2.text = temp[10]
                postedDate.text = temp[11]
                url.text = temp[12]
                rejectReason.text = temp[13]
                tree = ET.ElementTree(jobsroot)
                tree.write(XML_FILE_LOCATION())

                #Add back into main array
                temp[0] = self.__counter
                self.__treeRows.append(temp)

                #Put back into tree
                self.__tree.insert('', 'end', str(self.__counter), values=(temp[3], temp[5], temp[11]))
                self.__counter += 1

    def __RemoveCallBack(self):
        self.__RemoveItem()

    def __OpenURLCallBack(self):
        webbrowser.open_new_tab("https://www.seek.com.au" + self.__treeRows[int(self.__tree.focus())][12])
        
    def __ApplyToJobCallBack(self):
        coverLetterFile = open("Cover_Letter_Template", 'r')
        content = ""
        for c in coverLetterFile:
            content += c
        self.curItem = self.__tree.item(self.__tree.focus())
        pyperclip.copy(content.replace("{JOB_TITLE}", self.__treeRows[int(self.__tree.focus())][3]))
        webbrowser.open_new_tab("https://www.seek.com.au" + self.__treeRows[int(self.__tree.focus())][12])
        self.__RemoveItem()
