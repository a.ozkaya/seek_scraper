# Seek job filter v2.0 #
## What is it?  ##

This python3 program will get all of the jobs posted within 24 hours on Seek.com.au and filter out any jobs that are on the blacklist of words. It also works as a much quicker way to apply for jobs. <br>
Video showing project - [https://youtu.be/7X3DUdlHGGg ](https://youtu.be/7X3DUdlHGGg )

## External Python3 Libraries used ##
- pyperclip 1.7.0 
- beautifulsoup4 4.8.0
- boto3 1.10.47
- paramiko 2.7.1

## Notes: ##
- Run "JobClient.py" to run the client
    - NOTE: Client wont be able to get information from the server as .pem key isn't uploaded to Git
- Serverside folder is for the server side, it will run every night to get the jobs uploaded during the day and save it in an CSV format

## How the client side works: ##
On the **Recent Jobs** tab, there are 4 buttons<br>
**Apply:** Opens the highlighted job in the default browser, adding the job title to the cover letter and adding it to the paste bin and removing the job from the list <br>
**Remove:** Removes the highlighted job from the list<br>
**Open URL:** Opens the highlighted job in the default browser<br>
**Undo:** Re-adds any removed jobs <br>
![](READMEPICS/tab1.png) <br>
On the **Extra** tab, it will show all rejected jobs and the blacklist words<br>
![](READMEPICS/tab2.png) <br>

## Progess: ##
**In Progress**

- N/A
    
**To do:**
 
- **FEATURE**: Change cover letter based on category of job
- **FEATURE**: Add offline mode, server scripts will be update to get latest from last search

**Future Improvements:**

- **Improvement**: Speed up server side processing with threading

**Finished:**

- Use web scraper to get job listing from Seek.com.au
- Filter jobs out using words in a blacklist
- Upload results to a mySQL server
- **Client**: Get data from server
- Filter jobs in my SQL based on either being rejected, to be checked by user and checked by user
- **Client**: Add way to update blacklist words
- **Client**: Add way to check rejected jobs list
- **BUG**: Bold bullet pointed area isn't been checked on webpage: https://www.seek.com.au/job/40650830?type=standout#searchRequestToken=6b3fd6c8-c72b-4f4e-b725-c3d4f9ce0609
- **Client**: To open job URL and copying filled in cover letter to clipboard
- **Client**: Update SQL databased after applying for job
- **BUG**: Go to next seek page if needed/auto page
- **BUG**: Make sure script doesn't get same job twice
    - Since script runs once everyday, this might not be needed. Needs testing first
- **Client**: Clean GUI
- Add server side files to AWS Linux server
- **BUG**: Fix label on click of apply button
- **Client**: Add remove job button, puts job into rejects
- **Client**: Select many to remove at once
- **BUG**: Upload updated blacklist file to server when saved
- **DATA/SERVER**: Change to export to CSV on server and transfer file to client machine via client window. Will update local server storage. This is due to Amazon data bases costing money and I don't have any :(
- **BUG**: Fix remove of files in coversion script
- **BUG**: User caused reject error not showing
- **Client**: After testing, do redesign
    - Tab1_RecentJobs.py
        - Re-design to be vertical
        - Add button to update local database from server
        - Add button for quick open link, doesn't remove job from list or copy cover letter (changed copyURL to do this function, new name is openURL)
    - Tab2_BlackList_RejectList to be renamed to Tab2_RjectList
        - Remove Blacklist
        - Re-design to be vertical
        - Rename button "Copy URL" to "Open URL" and upload function
        - Add UpdateBlacklist button and function
    - Tab3_BlackList to be created
- **Improvement**: Small Screen mode - No needed, it's just small by default now
- **Improvement**: Apply button will open link to apply page instead of job page - Rejected, bad idea
- **SEVER/BUG**: Don't overwright CSV file on server side, update "UPDATEDB" to use all file from different dates
    - CSV is no longer over written everyday, its saved as JobData[Location]_[date].csv. Client has been updated to download many files with different names
- **MAY 2020 V2** Remove SQL data base (was collecting useless info) and only keep imported jobs that haven't been used yet. Replaced with XML
- **MAY 2020 V2** Have client check server for new data on load up instead of having "Update DB" button
- **MAY 2020 V2** Created popout box to see extra detail, now double click for more info
- **MAY 2020 V2** Check imports on all files
- **MAY 2020 V2** **BUG**: Blacklist words is read from local, should be from server
- **MAY 2020 V2** See rejected jobs but not store in a database or file for long term
- **MAY 2020 V2** Create way to undo last action
- **MAY 2020 V2** Create Loading screen on start up
- **MAY 2020 V2** Update README with new info
- **FEB 2021 V2.1** Seek changed class names so scraper didn't work, updated scraper to use better tags
- **FEB 2021 V2.1** Reformatted Server side code
- **FEB 2021 V2.1 BUG**: Blacklist doesn't update from client to server
- **FEB 2021 V2.1 BUG**: Reject Reason error sometimes doesn't come up, not program breaking
