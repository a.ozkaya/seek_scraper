from Imports import get, headers, BeautifulSoup
import os

def CheckBlackList(blackList, string):
    reasons = ""
    for b in blackList:
        if b in string:
            reasons += b + "\n"
            
    if (reasons != ""):
        return [True, reasons]
    return [False]

def ImportBlackListedWords(fileName):
    blackListFile = open(fileName, 'r')
    blackListWords = blackListFile.readlines()
    blackListFile.close()
    for i in range(len(blackListWords)):
        blackListWords[i] = blackListWords[i].split("\n")[0].lower()
    return blackListWords

def SortQuickDesciption(jobList):
    print("Performing light search based on black listed words")
    blacklistWords = ImportBlackListedWords("./BlacklistWords")
    removedJobs = []
    keptJobs = []
    for j in jobList:
        if (j["description1"] != None):
            checkDes1 = CheckBlackList(blacklistWords, j["description1"].lower())
            checkDes2 = CheckBlackList(blacklistWords, j["description2"].lower())
            rejectReason = ""
            if (checkDes1[0] or checkDes2[0]):
                if checkDes1[0]:
                    rejectReason += checkDes1[1]
                if checkDes2[0]:
                    rejectReason += checkDes2[1]
                j["rejected"] = True
                j["rejectedReason"] = rejectReason
                print(j)
                removedJobs.append(j)
            else:
                j["rejected"] = False
                j["rejectedReason"] = None
                keptJobs.append(j)

    return[keptJobs, removedJobs]

def SortFullScanDesciption(jobList):
    print("Performing deep search based on black listed words")
    blacklistWords = ImportBlackListedWords("./BlacklistWords")
    removedJobs = []
    keptJobs = []
    #For each job
    for job in jobList:
        response = get("https://www.seek.com.au"+job["jobURL"], headers=headers)
        html_soup = BeautifulSoup(response.text, 'html.parser')
        content1 = html_soup.find("div", {"id": "app"})
        check = CheckBlackList(blacklistWords, str(content1).lower())
        if check[0]:
            job["rejected"] = True
            job["rejectedReason"] = check[1]
            removedJobs.append(job)
        else:
            keptJobs.append(job)
    return [keptJobs, removedJobs]
