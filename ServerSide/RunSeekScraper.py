from Imports import sys
from ScapeSite import SearchJobs
from JobSorter import SortQuickDesciption, SortFullScanDesciption
from Exporter import ExportData
from datetime import datetime

#Main code

class App:
    def __init__(self):
        #Get jobs from seek
        jobs = SearchJobs(sys.argv[1])
        print(str(len(jobs)) + " jobs found...")
        #Do quick scan on all jobs of both format types
        jobsQS = SortQuickDesciption(jobs)
        print(str(len(jobsQS[0])) + " jobs passed quick search")
        #Do deep scan on jobs that passed above scan
        jobsDS = SortFullScanDesciption(jobsQS[0])
        print(str(len(jobsDS[0])) + " jobs passed deep search")
        #Export data (Good, bad)
        print("Exporting to file...")
        ExportData(jobsDS[0], jobsQS[1] + jobsDS[1], sys.argv[2])
        print("Export done")
        print(str(len(jobsDS[0])) + " passed searches")
        print(str(len(jobsDS[1]) + len(jobsQS[1])) + " failed searches")
        
if __name__ == "__main__":
    app = App()
