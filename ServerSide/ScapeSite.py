from Imports import BeautifulSoup, get, headers
from datetime import datetime

def GetJobInfo(divs):
    allJobInfo = []
    for div in divs:
        jobInfo = {
            "jobTitle": "",
            "companyName": "",
            "location": "",
            "pay": "",
            "category": "",
            "description1": "",
            "description2": "",
            "featured": False,
            "postedDate": str(datetime.date(datetime.now())),
            "jobURL": "",
        }

        var = div.find("span", {"data-automation": "jobListingDate"})
        if "d" in var.getText():
            return {
                "jobsList": allJobInfo, 
                "hitEnd": True
                }

        var = div.find("a", {"data-automation": "jobTitle"})
        if (var == None): jobInfo["jobTitle"] = None 
        else: jobInfo["jobTitle"] = var.getText()

        var = div.find("a", {"data-automation": "jobCompany"})
        if (var == None): jobInfo["companyName"] = None 
        else: jobInfo["companyName"] = var.getText()

        var = div.find("a", {"data-automation": "jobLocation"})
        if (var == None): jobInfo["location"] = None 
        else: jobInfo["location"] = var.getText()

        var = div.find("a", {"data-automation": "jobSalary"})
        if (var == None): jobInfo["pay"] = None 
        else: jobInfo["pay"] = var.getText()

        var = div.find("a", {"data-automation": "jobClassification"})
        if (var == None): jobInfo["category"] = None 
        else: jobInfo["category"] = var.getText()

        var = div.find("ul", {"class": "_3uiq0PN"})
        if (var == None): jobInfo["description1"] = None 
        else: jobInfo["description1"] = var.getText()

        var = div.find("span", {"data-automation": "jobShortDescription"})
        if (var == None): jobInfo["description2"] = None 
        else: jobInfo["description2"] = var.getText()

        var = div.find("a", {"data-automation": "jobPremium"})
        if (var == None): jobInfo["featured"] = False 
        else: jobInfo["featured"] = True

        var = div.find("a", {"data-automation": "jobTitle"})
        if (var == None): jobInfo["jobURL"] = None 
        else: jobInfo["jobURL"] = div.find("a", {"data-automation": "jobTitle"})["href"]

        allJobInfo.append(jobInfo)
    return {
        "jobsList": allJobInfo, 
        "hitEnd": False
        }
    

def SearchJobs(link_):
    returnArray = []
    loopFlag = True
    pageCounter = 1
    while loopFlag:
        link1 = link_.split("£")[0] + str(pageCounter)
        print("Scanning following link\n" + link1 + "\n")
        response = get(link1, headers=headers)
        html_soup = BeautifulSoup(response.text, 'html.parser')
        jInfo = GetJobInfo(html_soup.find_all("article", {"data-automation": "normalJob"}))
        for j in jInfo["jobsList"]:
            returnArray.append(j)
        if (jInfo["hitEnd"] == True):
            break
        pageCounter += 1
    return returnArray
